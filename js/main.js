/*global require*/

require.config({
    baseUrl: '',
    paths: {
        jquery: 'bower_components/jquery/dist/jquery',
        angular: 'bower_components/angular/angular.min',
        'angular-route': 'bower_components/angular-route/angular-route.min',
        app: "js/app",
        headerController: 'js/headerController',
        customersController: 'js/customersController',
        customersService: 'js/customersService'
    },
    shim: {
        'angular': {
            exports: 'angular'
        }
    }
});

require(['app'], function (app) {
    'use strict';

});