/*global define*/

define([], function () {
    'use strict';

    function customersController($scope, customersService) {

        customersService.getAll().then(function (data) {
            $scope.customers = data;
        });

        $scope.detail = function (id) {
            customersService.get(id).then(function (data) {
                window.console.log(data);                
            });
        };

    }

    return customersController;

});