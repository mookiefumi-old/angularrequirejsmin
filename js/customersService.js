/*global define*/

define([], function () {
    'use strict';

    var customersService = function ($http, $q) {

        function getData(url) {
            var deferred = $q.defer();
            $http.get(url).success(function (data) {
                deferred.resolve(data);
            }).error(function () {
                deferred.reject();
            });
            return deferred.promise;
        }

        return {
            getAll: function () {
                var url = "http://jsonplaceholder.typicode.com/users";
                return getData(url);
            },
            get: function (id) {
                var url = "http://jsonplaceholder.typicode.com/users/" + id;
                return getData(url);
            }
        };

    };

    return customersService;

});