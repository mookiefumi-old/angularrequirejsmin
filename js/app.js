/*global define*/

var dependecies = ['angular', 'headerController', 'customersController', 'customersService'];

define(dependecies, function (angular, headerController, customersController, customersService) {
    'use strict';
    
    var mookie = angular.module('mookie', []);
    
    mookie.controller('headerController', headerController);
    mookie.controller('customersController', customersController);
    mookie.factory('customersService', customersService);

});